import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({providedIn:'root'})
export class FilterService {
    
    constructor(private http: HttpClient) { }

    getClientsLarge() {
        return this.http.get<any>('https://jsonplaceholder.typicode.com/users')
        .toPromise()
            
    }
}
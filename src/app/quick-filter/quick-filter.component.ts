import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Client } from './quickFilter';
import { FilterService } from './quickFilterService';





@Component({
  selector: 'quickFilter',
  templateUrl: './quick-filter.component.html',
  styleUrls: ['./quick-filter.component.css']
})
export class QuickFilterComponent implements OnInit {
  clients: Client[];
  loading: boolean = true;


  

  constructor(private filterService: FilterService) { }


  ngOnInit() {

    this.filterService.getClientsLarge().then(clients => {
      this.clients = clients;
      this.loading = false;
      console.log(this.clients);
    },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      });

  }
}

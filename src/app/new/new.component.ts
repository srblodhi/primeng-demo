import { Component, OnInit } from '@angular/core';

import {SelectItem} from 'primeng/api';


interface OtherOptions {
  name: string,
  code: string
}

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  options: OtherOptions[];

  selectedoption: OtherOptions;

  items: SelectItem[];

  item: string;

  constructor() {
    this.items = [];
        for (let i = 0; i < 10000; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }

        this.options = [
            {name: 'Contact Customer', code: 'CC'},
            {name: 'Report Customer', code: 'RC'},
            
        ];
   }

  ngOnInit(): void {
  }

}

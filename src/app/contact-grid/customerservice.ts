import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable()
export class CustomerService {
    
    constructor(private http: HttpClient) { }

    getCustomersLarge() {
        return this.http.get<any>('https://jsonplaceholder.typicode.com/posts')
        .toPromise()
            
    }
}
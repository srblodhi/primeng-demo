import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Customer} from './customer';
import { CustomerService } from './customerservice';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-contactgrid',
  templateUrl: './contact-grid.component.html',
  styleUrls: ['./contact-grid.component.css']
})
export class ContactGridComponent implements OnInit {
  customers: Customer[];
  loading: boolean = true;
  

  @ViewChild('dt') table: Table;

  constructor (private httpService: HttpClient, private customerService: CustomerService, private primengConfig: PrimeNGConfig) { }
  arrContacts: string [];

  ngOnInit () {
    /*this.httpService.get('https://jsonplaceholder.typicode.com/posts').subscribe(
      data => {
        this.arrContacts = data as string [];	 // FILL THE ARRAY WITH DATA.
        //  console.log(this.arrContacts[1]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );*/
    this.customerService.getCustomersLarge().then(customers => {
      this.customers = customers;
      this.loading = false; 
      console.log(this.customers);
  },
  (err: HttpErrorResponse) => {
    console.log (err.message);
  });
this.primengConfig.ripple = true;
}
}

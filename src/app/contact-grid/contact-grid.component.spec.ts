import { ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ContactGridComponent } from './contact-grid.component';
import {CustomerService} from './customerservice'



describe('ContactGridComponent', () => {
  let component: ContactGridComponent;
  let fixture: ComponentFixture<ContactGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactGridComponent ],
     
      
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});



describe('CustomerService', () => {
  let service: CustomerService;
  let serviceSpy: jasmine.SpyObj<CustomerService>;
  beforeEach(() => {
    const spy = jasmine.createSpyObj('CustomerService', ['getCustomersLarge']);
    TestBed.configureTestingModule({
      providers: [
        CustomerService,
        { provide: CustomerService, useValue: spy }
      ]
    });

    // Inject both the service-to-test and its (spy) dependency
  service = TestBed.inject(CustomerService);
  serviceSpy = TestBed.inject(CustomerService) as jasmine.SpyObj<CustomerService>;
  

  it('should use ValueService', () => {
    let service = TestBed.inject(CustomerService);
    expect(service.getCustomersLarge()).toHaveBeenCalledTimes(1);
  });
  
});



})